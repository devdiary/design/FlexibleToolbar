### [DeveloperDiary](https://gitlab.com/devdiary)
## [Design](https://gitlab.com/devdiary/design)
# FlexibleToolbar
Flexible toolbar without support library and hard api limitations

<div align="center">
  <img src="media/icon.png"/>
</div>
<div align="center">
  <strong>source for <a href="https://medium.com/developerdiary/design">medium</a></strong>
</div>
