package devdiary.design.flxtlb;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

public class MainActivity
        extends Activity
{
    private float density;

    private ScrollView scroll;
    private View toolbar_container;

    private float toolbarMaxHeight;

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.main_screen);
        density = getResources().getDisplayMetrics().density;
        scroll = findViewById(R.id.scroll);
        toolbar_container = findViewById(R.id.toolbar_container);
        toolbarMaxHeight = px(128);
        ViewGroup.LayoutParams layoutParams = toolbar_container.getLayoutParams();
        layoutParams.height = (int)toolbarMaxHeight;
        toolbar_container.setLayoutParams(layoutParams);
        scroll.setClipToPadding(false);
        scroll.setPadding(0, (int)toolbarMaxHeight, 0, 0);
    }

    protected final int px(float dp)
    {
        if(dp < 0)
        {
            return 0;
        }
        return (int)Math.ceil(density * dp);
    }
}